import { LitElement, html } from '../js/lit-element.js'

export class RedButton extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  static get properties() {
    return {
      status: { type: Number }
    }
  }

  constructor() {
    super()
    Puck.write('LED1.reset();\n')
    this.status = 0
  }
  
  render(){
    return html`
      <button @click="${this.onClick}">
        🔴 Click me!
      </button>
    `
  }

  onClick() {
    if(this.status==0) {
      Puck.write('LED1.set();\n')
      this.status=1
    } else {
      Puck.write('LED1.reset();\n')
      this.status=0
    }
  }
}
customElements.define('red-button', RedButton)
